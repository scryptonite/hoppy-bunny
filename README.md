Hoppy Bunny
===========
<sup>*teamTALIMA Project #1 &mdash; [Developed LIVE on twitch.tv/teamTALIMA](https://www.twitch.tv/teamtalima)*</sup>

A 2D mobile platform game based on Flappy Bird. :rabbit:

The Android APKs and other platform builds are located under [`/Builds`][builds]. [Download v0.2 APK][download_apk]


## Specs

- Unity 5.5.0f3
- Programmed in C#


## Needed Contributions

Feel free to submit a merge request!

- *Sound Effects*
    - [ ] Death by running into obstacle
    - [ ] Score increment
    - [ ] Bunny hop


## Talima Todo
- [x] Game build for Android: [Find the lastest builds here!][builds]
- [x] Finishing UI animations and SFX
- [ ] 

[builds]: https://gitlab.com/teamTALIMA/hoppy-bunny/tree/master/Builds
[download_apk]: https://gitlab.com/teamTALIMA/hoppy-bunny/blob/master/Builds/hoppy-bunny_v0.2.apk