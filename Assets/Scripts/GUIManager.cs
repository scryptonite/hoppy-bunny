﻿/* REVIEWED & COMPLETED
 * 12/30/16
 */

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class GUIManager : MonoBehaviour {

    // Singleton
    public static GUIManager instance = null;

    // GameManager reference
    GameManager _gm;

    // Main Menu UI
    [Header("Main Menu UI")]
    public GameObject canvasMainMenu;
    public Button btnStart;
    //public Button btnRate;
    public GameObject highScoreMenu;
    public Text highScoreNum1;
    public Text highScoreNum2;
    public Text highScoreNum3;
    public Text totalObstacles;

    // Game UI
    [Header("Game UI")]
    public GameObject canvasGameLevel;
    public Button btnPause;
    public Text scoreText;

    // Game End UI
    [Header("Game End UI")]
    public Animator gameEndMenuAnimator;
    public Text curScoreNum;
    public Text highScoreNum;
    public GameObject newHighScoreRed;
    public GameObject btnsGameOver;
    public Button btnOK;
    //public Button btnShare;

    void Awake() {
        // Check if instance already exists, if not set instance to 'this', if instance is not 'this' destory 'this'
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

        // Sets this gameObject to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);
    }
    
    void Start () {
        _gm = FindObjectOfType<GameManager>();

        // Main Menu Setup
        btnStart.onClick.AddListener(_gm.PlayGame);
        //btnRate.onClick.AddListener(_gm.XXX);

        // Score UI Menu
        highScoreNum1.text = PlayerPrefs.GetInt("Highscore1").ToString();
        highScoreNum2.text = PlayerPrefs.GetInt("Highscore2").ToString();
        highScoreNum3.text = PlayerPrefs.GetInt("Highscore3").ToString();
        totalObstacles.text = PlayerPrefs.GetInt("TotalObstacles").ToString();

        // Game Level Setup
        btnPause.onClick.AddListener(_gm.Pause);
        btnOK.onClick.AddListener(_gm.Restart);
        //btnShare.onClick.AddListener(_gm.XXX);
    }

    public void UpdateScore(int score) {
        scoreText.text = score.ToString();
    }

    public void GameOver(int score, bool newScore) {
        // At 0.0f, Deactivate score UI and Pause Button
        scoreText.gameObject.SetActive(false);
        highScoreNum.GetComponent<Text>().text = PlayerPrefs.GetInt("Highscore1").ToString();
        if (newScore) {
            newHighScoreRed.SetActive(true);
        }
        btnPause.gameObject.SetActive(false);

        // Activate 'gameOverEvent' animation and sfx
        gameEndMenuAnimator.gameObject.SetActive(true);
        gameEndMenuAnimator.SetBool("gameOverEvent", true);

        // At 1.8f, Tally curScoreNum from 0 to scoreText and completionCallback to Enable buttons
        StartCoroutine(GameOverTally(curScoreNum, score, .5f, 1.8f, () => {
            btnsGameOver.SetActive(true);
            _gm.ads.ShowBannerBottom();
        }));
    }

    IEnumerator GameOverTally(Text ourNum, int total, float duration, float delay, Action completionCallback) {
        float totalTime = 0;
        float curDelayTime = 0;

        // Wait for delay
        while (curDelayTime < delay) {
            curDelayTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        // Tally from 0 to 'total' for 'duration'
        while (totalTime <= duration) {
            totalTime += Time.deltaTime;
            ourNum.text = Mathf.CeilToInt(Mathf.Lerp(0, (float)total, totalTime / duration)).ToString();
            yield return new WaitForEndOfFrame();
        }
        ourNum.text = total.ToString();

        // Complete callback (enable buttons)
        completionCallback();
        yield break;
    }

    public void MainMenuLoad() {
        btnsGameOver.SetActive(false);
        _gm.ads.RemoveAd();

        canvasGameLevel.SetActive(false);
        canvasMainMenu.SetActive(true);
        
        highScoreNum1.text = PlayerPrefs.GetInt("Highscore1").ToString();
        highScoreNum2.text = PlayerPrefs.GetInt("Highscore2").ToString();
        highScoreNum3.text = PlayerPrefs.GetInt("Highscore3").ToString();
        totalObstacles.text = PlayerPrefs.GetInt("TotalObstacles").ToString();

        // Show Banner Ad at top of screen
        _gm.ads.ShowBannerTop();
    }

    public void GameLevelLoad() {
        _gm.ads.RemoveAd();

        canvasMainMenu.SetActive(false);
        canvasGameLevel.SetActive(true);

        btnPause.gameObject.SetActive(true);
        scoreText.gameObject.SetActive(true);
        scoreText.text = "0";
        curScoreNum.text = "0";
        newHighScoreRed.gameObject.SetActive(false);

        // TODO: Add instruction icons
    }
}