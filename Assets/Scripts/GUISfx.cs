﻿/* REVIEWED & COMPLETED
 * 12/17/16
 */

using UnityEngine;

public class GUISfx : MonoBehaviour {

    public AudioSource _gameOverAudio;
    public AudioSource _gameEndMenuAudio;

    // GameOver Text translate
    public void GameOverTextSFX() {
        _gameOverAudio.Play();
    }

    // GameEndMenu translate
    public void GameEndMenuSFX() {
        _gameEndMenuAudio.Play();
    }
}