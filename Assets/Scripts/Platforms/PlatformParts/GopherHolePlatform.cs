﻿/* REVIEWED & COMPLETED
 * 1/5/16
 */

using UnityEngine;

public class GopherHolePlatform : PlatformComponent {
    // Platform w/ GopherHole obstacle

    float minX = -3.5f;
    float maxX = 3.5f;
    float obstacleY = .5f;

    float[] _lastPos = new float[2];
    float _bound;
    float _offset = 0.1f;

    // Triggered through SendMessage by PlatformManager in PlatformGeneratorCoroutine
    void PlatformSetup() {
        // Determine # of obstacles for the platform
        int rand = Random.Range(0, transform.childCount);

        // Determine width of obstacles
        float _bound = transform.GetChild(0).GetComponent<SpriteRenderer>().bounds.size.x;
        Debug.Log("NEW PLATFORM: bound = " + _bound);

        // For obstacle (transform child) {
        for(int t = 0; t < transform.childCount; t++) {
            Transform child = transform.GetChild(t);

            // Activate 'rand' amount of obstacles and set localPosition
            child.gameObject.SetActive(child.GetSiblingIndex() <= 2);
            if (child.gameObject.activeSelf) {
                child.localPosition = new Vector2(Random.Range(minX, maxX), obstacleY);
                Debug.Log("localPosition of child(" + child.GetSiblingIndex() + "): " + child.localPosition.x);

                // Ensure minimal overlapping,
                // If child.lP is greater than (_lastPosition - bounds) AND lesser than (_lastPosition + bounds), then...
                // it overlaps with the previous obstacle
                if (_lastPos != null) {
                    for (int i = 0; i < _lastPos.Length; i++) {
                        if (_lastPos[i] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[i] + _bound) {
                            // If child.lP is less than _lastPos, then it overlaps on the left so move it's 'x' position down
                            if (child.localPosition.x < _lastPos[i]) {
                                float overlap = _bound - (_lastPos[i] - child.localPosition.x);
                                child.localPosition = new Vector2(child.localPosition.x - overlap, obstacleY);
                                Debug.Log("CONFLICT lesser: new position = " + child.localPosition.x);

                                // If child.lP overlaps second obstacle, then move obstacle over to left
                                if (i == 0 && _lastPos[1] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[1] + _bound) {
                                    Transform obst = transform.GetChild(1);
                                    float obstOverlap = _bound - (child.localPosition.x - obst.localPosition.x);
                                    obst.localPosition = new Vector2(obst.localPosition.x - obstOverlap, obstacleY);
                                    Debug.Log("SECOND CONFLICT lesser: obst new position = " + obst.localPosition.x);
                                } else if (_lastPos[0] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[0] + _bound) {
                                    Transform obst = transform.GetChild(2);
                                    float obstOverlap = _bound - (child.localPosition.x - obst.localPosition.x);
                                    obst.localPosition = new Vector2(obst.localPosition.x - obstOverlap, obstacleY);
                                    Debug.Log("SECOND CONFLICT lesser: obst new position = " + obst.localPosition.x);
                                }
                                // Else, child.lP is greater than _lastPos, and it overlaps on the right so move it's 'x' position up
                            } else {
                                float overlap = _bound - (child.localPosition.x - _lastPos[i]);
                                child.localPosition = new Vector2(child.localPosition.x + overlap, obstacleY);
                                Debug.Log("CONFLICT greater: new position = " + child.localPosition.x);

                                // If child.lP overlaps second obstacle, then move obstacle over to right
                                if (i == 0 && _lastPos[1] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[1] + _bound) {
                                    Transform obst = transform.GetChild(1);
                                    float obstOverlap = _bound - (obst.localPosition.x - child.localPosition.x);
                                    obst.localPosition = new Vector2(obst.localPosition.x + obstOverlap, obstacleY);
                                    Debug.Log("SECOND CONFLICT greater: obst new position = " + obst.localPosition.x);
                                } else if (_lastPos[0] - _bound < child.localPosition.x && child.localPosition.x < _lastPos[0] + _bound) {
                                    Transform obst = transform.GetChild(2);
                                    float obstOverlap = _bound - (obst.localPosition.x - child.localPosition.x);
                                    obst.localPosition = new Vector2(obst.localPosition.x + obstOverlap, obstacleY);
                                    Debug.Log("SECOND CONFLICT greater: obst new position = " + obst.localPosition.x);
                                }
                            }
                        }
                    }
                }

                if(t < 2) {
                    _lastPos[t] = child.localPosition.x;
                    Debug.Log("_lastPos[" + t + "]: " + _lastPos[t]);
                }
            }
        }
    }

    public void NewObstPos(Vector3 newObstPos, float overlappingObst, bool right = true) {
        if(right) {
            float obstOverlap = _bound - (newObstPos.x - overlappingObst);
            newObstPos = new Vector2(newObstPos.x + obstOverlap - _offset, obstacleY);
        } else {
            float obstOverlap = _bound - (overlappingObst - newObstPos.x);
            newObstPos = new Vector2(newObstPos.x - obstOverlap + _offset, obstacleY);
        }
    }
}



//float x = minX;
//// width of the gopher hole sprite:
//float width = 1.2f;
//// remaining space:
//float remaining = Mathf.Abs(minX - maxX);
//float y = 0.6f;

//foreach (Transform child in transform) {
//    int childIndex = child.GetSiblingIndex();
//    bool isActive = childIndex <= rand;
//    child.gameObject.SetActive(isActive);
//    if (!isActive) continue;
//    float fraction = (float)((rand - childIndex) + 1);
//    // random value based on what would be a "fair" portion of the remaining space:
//    float randX = Random.Range(0f, remaining / fraction);
//    remaining -= randX + width;
//    x += randX;
//    child.localPosition = new Vector2(x, y);
//}