﻿/* REVIEWED & COMPLETED
 * 12/17/16
 */

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class PlayerController : MonoBehaviour {

    GameManager _gm;

    // Player variables
    Rigidbody2D _rb;
    SpriteRenderer _sprite;
    int _jumpCount;
    public int jumpMax = 8;
    public float speed = 5;
    public float jumpHeight = 2;
    public float fallSpeed = -12;
    public float topRotation = 20f;

    public AudioSource _hopAudio;
    public AudioSource _hopAudio2;
    public AudioSource _dieAudio;

    // Level variables
    public bool _hasInput = false;
    bool _lose = false;
    
	void Start () {
        _gm = FindObjectOfType<GameManager>();
        _rb = GetComponent<Rigidbody2D>();
        _sprite = GetComponent<SpriteRenderer>();
    }
    
    void Update() {
        if (!_gm.pause && !_lose) {
            // TODO: Improve Input condition
            // Get 'tap' input
            if ((Input.GetKeyDown("space") || Input.GetMouseButtonDown(0) /*|| Input.touchCount >= 1*/) && _jumpCount < jumpMax) {
                var pointer = new PointerEventData(EventSystem.current);
                pointer.position = Input.mousePosition; // improve for mobile

                var hits = new List<RaycastResult>();
                EventSystem.current.RaycastAll(pointer, hits);

                if (!hits.Any(hit => hit.gameObject.CompareTag("Pause"))) {
                    _rb.velocity = new Vector2(speed, jumpHeight);
                    _rb.rotation = topRotation;

                    if (_jumpCount == jumpMax - 3 || _jumpCount == jumpMax - 2) {
                        _sprite.color = new Color(255/255f, 180/255f, 180/255f, 255/255f);
                        _hopAudio2.Play();
                    } else if (_jumpCount >= jumpMax - 1) {
                        _sprite.color = new Color(255/255f, 110/255f, 110/255f, 255/255f);
                        _hopAudio2.Play();
                    } else {
                        _hopAudio.Play();
                    }

                    _jumpCount++;
                    _hasInput = true;
                }
            }
        }
    }

    void FixedUpdate() {
        // Bunny (player) constantly moves forward && Clamp fall speed
        if(!_lose) _rb.velocity = new Vector2(speed, Mathf.Clamp(_rb.velocity.y, fallSpeed, 100));

        // If we are falling, then rotate downwards
        if (_rb.velocity.y < -8 && _rb.rotation > -90) {
            _rb.rotation = Mathf.LerpAngle(_rb.rotation, Mathf.Clamp(_rb.rotation - 15f, -90f, topRotation), .5f);
        }
    }

    void OnTriggerEnter2D(Collider2D other) {
        // If we pass an obstacle 'trigger', then add to score
        if (other.gameObject.CompareTag("Obstacle")) {
            _gm.SendMessage("AddScore", SendMessageOptions.DontRequireReceiver);
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("Platform")) {
            _jumpCount = 0;
            _sprite.color = new Color(255, 255, 255, 255);
        } else if (collision.gameObject.CompareTag("Obstacle")) {
            // If we collide with an obstacle, then we lose
            _lose = true;
            _rb.velocity = new Vector2(0, jumpHeight);
            _rb.rotation = topRotation;
            _dieAudio.Play();

            // Disable obstacle's colliders (allows player to fall through obstacle to ground/platform
            Collider2D[] colliders = collision.gameObject.GetComponentInParent<PlatformComponent>().GetComponentsInChildren<Collider2D>();
            foreach (Collider2D collider in colliders) {
                if (collider.GetComponent<PlatformComponent>() == null) {
                    collider.enabled = false;
                }
            }

            _gm.Lose();
        }
    }
}