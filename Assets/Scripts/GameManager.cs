﻿/* REVIEWED & COMPLETED
 * 12/16/16
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using AndroidGoodiesExamples;

public class GameManager : MonoBehaviour {

    // Singleton
    public static GameManager instance = null;

    // GUIManager reference
    GUIManager _guim;

    // Google Ads Script
    public GoogleAds ads;

    public Image faderImage; //
    public CanvasGroup faderCanvas; //
    public int fadeSpeed = 1; //
    public int flashSpeed = 10; //
    AsyncOperation async;

    // Game Level Variables
    public bool pause = false;

    PlayerController _player;
    PlatformManager _platformManager;
    bool _changedGenerator = false;
    int _score = 0;
    AudioSource _scoreAudio;

    void Awake() {
        // Check if instance already exists, if not set instance to 'this', if instance is not 'this' destory 'this'
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);

        // Sets this gameObject to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        // If no Highscore, set Highscore to 0
        if (!PlayerPrefs.HasKey("Highscore1")) {
            PlayerPrefs.SetInt("Highscore1", 0);
        }
        if (!PlayerPrefs.HasKey("Highscore2")) {
            PlayerPrefs.SetInt("Highscore2", 0);
        }
        if (!PlayerPrefs.HasKey("Highscore3")) {
            PlayerPrefs.SetInt("Highscore3", 0);
        }
        if (!PlayerPrefs.HasKey("TotalObstacles")) {
            PlayerPrefs.SetInt("TotalObstacles", 0);
        }
    }

    void Start() {
        _guim = FindObjectOfType<GUIManager>();
        _scoreAudio = GetComponent<AudioSource>();
        ads = GetComponent<GoogleAds>();
    }

    void Update() {
        // TODO: Improve GameManager Update() condition statement
        if (SceneManager.GetActiveScene().name == "Game Level" && _player != null && _player._hasInput && !_changedGenerator) {
            if (_platformManager != null) _platformManager.generatorMode = PlatformManager.GeneratorMode.Default;
            // TODO: Hide instructions
            _changedGenerator = true;
        }
    }

    public void AddScore() {
        // TODO: Some times double triggered
        _score += 1;
        _guim.UpdateScore(_score);
        _scoreAudio.Play();
    }

    public void Pause() {
        if (!pause) {
            Time.timeScale = 0.0f;
            pause = !pause;
        } else if (pause) {
            Time.timeScale = 1.0f;
            pause = !pause;
        }
    }

    public void Lose() {
        StartCoroutine(WhiteFlash());

        // Determine if we have a new highscore and save in PlayerPrefs
        bool newScore = false;
        if (_score > PlayerPrefs.GetInt("Highscore3")) {
            if (_score > PlayerPrefs.GetInt("Highscore2")) {
                if (_score > PlayerPrefs.GetInt("Highscore1")) {
                    PlayerPrefs.SetInt("Highscore3", PlayerPrefs.GetInt("Highscore2"));
                    PlayerPrefs.SetInt("Highscore2", PlayerPrefs.GetInt("Highscore1"));
                    PlayerPrefs.SetInt("Highscore1", _score);
                    newScore = true;
                } else {
                    PlayerPrefs.SetInt("Highscore3", PlayerPrefs.GetInt("Highscore2"));
                    PlayerPrefs.SetInt("Highscore2", _score);
                }
            } else {
                PlayerPrefs.SetInt("Highscore3", _score);
            }
        }

        // Update TotalObstacles count
        PlayerPrefs.SetInt("TotalObstacles", (_score + PlayerPrefs.GetInt("TotalObstacles")));
        _guim.GameOver(_score, newScore);
    }

    private IEnumerator WhiteFlash() {
        faderImage.color = Color.white;
        // Fade-in fader to alpha = 1
        while (faderCanvas.alpha < 0.9f) {
            faderCanvas.alpha = Mathf.Lerp(faderCanvas.alpha, 1, Time.deltaTime * flashSpeed);
            yield return null;
        }
        faderCanvas.alpha = 1;
        // Fade-in fader to alpha = 0
        while (faderCanvas.alpha > 0.1f) {
            faderCanvas.alpha = Mathf.Lerp(faderCanvas.alpha, 0, Time.deltaTime * flashSpeed);
            yield return null;
        }
        faderCanvas.alpha = 0;
        faderImage.color = Color.black;
    }



    //*** Scene Management ***//
    public void PlayGame() { // Main Menu 'Start' Button
        StartCoroutine(LoadNextLevel("Game Level"));
    }

    public void Restart() { // Game End Menu 'Restart' Button
        StartCoroutine(LoadNextLevel("Main Menu"));
    }

    private IEnumerator LoadNextLevel(string name) {
        // Fade-in fader to alpha = 1
        while (faderCanvas.alpha < 0.9f) {
            faderCanvas.alpha = Mathf.Lerp(faderCanvas.alpha, 1, Time.deltaTime * fadeSpeed);
            yield return null;
        }
        faderCanvas.alpha = 1;

        // Load scene
        async = SceneManager.LoadSceneAsync(name);

        // Wait until scene has loaded
        yield return new WaitUntil(() => async.isDone);

        if (name == "Game Level") GameLevelLoad();
        else if (name == "Main Menu") MainMenuLoad();

        // Fade-out fader to alpha = 0
        while (faderCanvas.alpha > 0.1f) {
            faderCanvas.alpha = Mathf.Lerp(faderCanvas.alpha, 0, Time.deltaTime * fadeSpeed);
            yield return null;
        }
        faderCanvas.alpha = 0;
    }

    void GameLevelLoad() {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        _platformManager = GameObject.FindObjectOfType<PlatformManager>();
        _changedGenerator = false;
        _guim.GameLevelLoad();
    }

    void MainMenuLoad() {
        _score = 0;
        _guim.MainMenuLoad();
    }



    //*** Rate & Share Functionality ***//
    public void OnRateClick() {
        StartCoroutine(RateUs());
    }

    public IEnumerator RateUs() {
        Animator anim = _guim.GetComponentInChildren<Animator>();
        anim.SetTrigger("ratePress");

        while (!anim.GetCurrentAnimatorStateInfo(0).IsName("ratePress")) {
            yield return null;
        }

        Application.OpenURL("market://details?id=com.supercell.clashofclans&hl=en");
    }

    public void OnShareClick() {
        StartCoroutine(ShareScore());
    }

    public IEnumerator ShareScore() {
        Animator anim = _guim.gameEndMenuAnimator;
        anim.SetTrigger("sharePress");

        //yield return new WaitUntil(() => !anim.IsInTransition(1));
        //yield return new WaitUntil(() => !anim.GetCurrentAnimatorStateInfo(1).IsName("sharePress"));

        while (!anim.GetCurrentAnimatorStateInfo(1).IsName("sharePress")) {
            yield return null;
        }

        this.GetComponent<NativeShareTest>().OnShareClick();
    }
}